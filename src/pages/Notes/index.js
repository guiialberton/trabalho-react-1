import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes} from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {

  state = {
    filtrado: [],
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    varDeBusca: '',
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false,
      filtrado: await listNotes()
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  _handleKeyPress = async (e) => {
    if (e.key === 'Enter') {
      this.setState({
        loadingSave: true
      })
      await saveNotes(this.state.text, this.state.index)
      this.setState({
        notes: await listNotes(),
        loadingSave: false,
        new: false,
        text: ''
      })
    }
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text, this.state.index)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: ''
    })
  }

  busca = async (varDeBusca) => {
    this.setState({
      filtrado: this.state.notes.filter(nota => nota.text.toLowerCase().search(varDeBusca.target.value.toLowerCase()) !== -1)
    })
  }

  editNote = async (index) => {
    if(index !== -1) {
      this.setState({
        text: this.state.notes[index].text,
        new: true,
        index: index
      })
    }
  }

  delNote = (index) => {
    delete this.state.notes[index]
      this.setState({
        notes: this.state.notes
      })
  }

  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
            <input type='search' onChange={this.busca} placeholder='Procurar...'/>
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input autoFocus value={this.state.text} onChange={this.editText} onKeyPress={this._handleKeyPress} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            { this.state.filtrado.map((note, index) => (
              <div key={note.id} className='item'>
                <input disabled className='left' type='text' value={note.text} />
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={() => this.delNote(index)}>Excluir</button>) }
                  { note.id && (<button onClick={() => this.editNote(index)}>Editar</button>) }
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
