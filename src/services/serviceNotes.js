const result = [
    { text: 'Lavar o carro', id: Math.random().toString(36).substr(2, 16) },
    { text: 'Dar vacina na Alice', id: Math.random().toString(36).substr(2, 16) }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 2000)
    })
}


const saveNotes = (text, index) => {
console.log("TCL: saveNotes -> index", index)

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (index >= 0) {
                result[index].text = text
              } else {
                result.unshift({
                  text: text,
                  id: Math.random().toString(36).substr(2, 16)
                })
              }
            resolve()
        }, 2000)
    })
}

module.exports = {
    listNotes,
    saveNotes
}